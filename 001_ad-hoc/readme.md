```
ansible all -a "sudo /sbin/reboot"
ansible all -m command -a "/sbin/reboot" -a -b
ansible all -m shell -a "/sbin/reboot" -a -b
ansible all -m reboot -b
ansible all -m shell -a "hostname"
ansible all -m hostname -a "name=matejmadeja" -b
ansible all -m hostname -a "name=node5" -b
ansible all -m copy -a "src=/tmp/test.txt dest=/tmp/haluz.txt"
ansible all -m file -a "path=/tmp/test state=directory"
ansible all -m shell -a "mv /tmp/haluz.txt /tmp/test/haluz.txt"
ansible all -m file -a "path=/tmp/test state=absent"
ansible all -m apt -a "name=cmatrix state=present" -b
```